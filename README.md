Neuron cellular morphology plays a huge role in the function of neurons as
information processing units. This lab will explore methods for processing
images to produce quantitative descriptions of neuron morphology and meso-scale
connectivity.

**Hands on Exercise:
**

Student will use neuTube and Fiji simple neurite tracer to perform
semi-automated tracing of fluorescently labeled neurons. Students will evaluate
the usability of these tools and their ability to handle very large image
volumes and develop strategies for partitioning data into sub-volumes,
semi-automated annotation of neurites and soma, and re-assembly of annotation
results into complete long-range traces. Students will examine use of these
methods for connectomics pipelines and fast search and retrieval in neuron
morphology databases.

**Key Concepts Learned:
**

Students will understand building blocks needed to construct a neuroinformatics
pipeline for optical tracing.

**References:
**

neuTube software tutorial
http://www.neutracing.com/tutorial/

Fiji Simple Neurite Tracer
http://imagej.net/Simple_Neurite_Tracer