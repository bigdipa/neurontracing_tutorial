
A = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/npf-M-200000.swc',' ',2,0);
A = A(:,3:5);
B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01747.swc',' ',2,0);
B = B(:,3:5);


B(:,3) = max(B(:,3))-B(:,3);
B(:,3) = 2.8*B(:,3);
%B = 0.3198*B;
B = 0.345*B;

B = B - repmat(mean(B),215,1);
B = B + repmat(mean(A),215,1);
B(:,3) = B(:,3)+15;

pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',18);
hold on;
pcshow(pcB,'MarkerSize',18);
whitebg(figure(1),'k')

figure(1); clf;
pcshow(pcA,'MarkerSize',18);
hold on;
trans = pcregrigid(pcB,pcA);
pcBW = pctransform(pcB,trans);
pcshow(pcBW,'MarkerSize',18);
whitebg(figure(1),'k')

% get complete transform 

B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01747.swc',' ',2,0);
B = B(:,3:5);
Bh = [B ones(size(B,1),1)];
BWh = [pcBW.Location ones(size(B,1),1)];

M = inv(BWh\Bh);


% verify

A = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/npf-M-200000.swc',' ',2,0);
A = A(:,3:5);
B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01747.swc',' ',2,0);
B = B(:,3:5);
Bh = [B ones(size(B,1),1)];
B = Bh*M;
B = B(:,1:3);

pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
pcshow(pcB,'MarkerSize',12);


% print out in allensdk format
sprintf('%2.3f, ',M(:,1:3)') % this no good, need to transpose




%
%
%

%
% ARGH!!!!   npf-M-300001 is mirrored?
%
A = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/npf-M-300001.swc',' ',2,0);
A = A(:,3:5);
B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01748.swc',' ',2,0);
B = B(:,3:5);


B(:,3) = max(B(:,3))-B(:,3);
B(:,3) = 2.8*B(:,3);
B = 0.345*B;

B = B - repmat(mean(B),size(B,1),1);
B = B + repmat(mean(A),size(B,1),1);
B(:,3) = B(:,3)+15;
B(:,1) = B(:,1)+50;

pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
pcshow(pcB,'MarkerSize',12);

figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
trans = pcregrigid(pcB,pcA);
pcBW = pctransform(pcB,trans);
pcshow(pcBW,'MarkerSize',12);

% get complete transform 

B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01748.swc',' ',2,0);
B = B(:,3:5);
Bh = [B ones(size(B,1),1)];
BWh = [pcBW.Location ones(size(B,1),1)];

M = inv(BWh\Bh);


% verify
B = dlmread('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/01748.swc',' ',2,0);
B = B(:,3:5);
Bh = [B ones(size(B,1),1)];
B = Bh*M;
B = B(:,1:3);

pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
pcshow(pcB,'MarkerSize',12);


% print out in allensdk format

sprintf('%2.3f, ',M(:,1:3)') %%% TODO: this is in error, need to transpose


