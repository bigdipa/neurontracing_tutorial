
%
% align tracings with flycircuit
%



F = {
'01747.swc','npf-M-200000.swc',
'01753.swc','npf-M-300004.swc' ,
'01755.swc','npf-M-300006.swc',
'01749.swc','npf-M-300002.swc' , %%%% these don't line up at all!!
'01756.swc','npf-M-100000.swc' , %%% nor do these, different scale? 
'01754.swc','npf-M-300005.swc' ,
'01748.swc','npf-M-300001.swc' ,
'01752.swc','npf-M-200002.swc',  % mirror x, mirror z .... still goofy
'01751.swc','npf-M-200001.swc' ,
}

figure(1); clf; hold on;
figure(2); clf; hold on;
figure(3); clf; hold on;
A = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/%s',F{l,2}),' ',2,0);
A = A(:,3:5);
B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s',F{l,1}),' ',2,0);
B = B(:,3:5);

if (l>7)
  B(:,3) = 2.8*B(:,3); %
  B(:,1) = -B(:,1); %
  B = 0.344*B;
  B(:,1) = B(:,1) + 465; 
  B(:,2) = B(:,2) - 30; 
  B(:,3) = B(:,3) - 5; %
else
  B(:,3) = -2.8*B(:,3);
  B = 0.344*B;
  B(:,1) = B(:,1) + 109; 
  B(:,2) = B(:,2) - 30; 
  B(:,3) = B(:,3) + 95; 
end

figure(3);
plot3(A(:,1),A(:,2),A(:,3),'b.'); %registered flycircuit
hold on;
plot3(B(:,1),B(:,2),B(:,3),'r.'); %traces
axis square; grid on;

print("select 4 points, blue then red");
X = getCursorInfo(datacursormode(figure(3)))
for i = 1:4
  XB{l}(i,:) = X(2*i-1).Position 
  XR{l}(i,:) = X(2*i).Position 
end
save clicks.mat XR XB

m = mean(XB{l}-XR{l})
figure(3);clf
plot3(A(:,1),A(:,2),A(:,3),'b.'); %registered flycircuit
hold on;
plot3(B(:,1)-m(1),B(:,2)-m(2),B(:,3)-m(3),'r.'); %traces
axis square; grid on;

B(:,1) = B(:,1)-m(1);
B(:,2) = B(:,2)-m(2);
B(:,3) = B(:,3)-m(3);
pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
pcshow(pcB,'MarkerSize',12);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
trans = pcregrigid(pcB,pcA);
pcBW = pctransform(pcB,trans);
pcshow(pcBW,'MarkerSize',12);

% get complete transform 
B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s',F{l,1}),' ',2,0);
B = B(:,3:5);
Bh = [B ones(size(B,1),1)];
BWh = [pcBW.Location ones(size(B,1),1)];
M = inv(BWh\Bh);


% verify
A = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/%s',F{l,2}),' ',2,0);
A = A(:,3:5);
B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s',F{l,1}),' ',2,0);
B = B(:,3:5);

Bh = [B ones(size(B,1),1)];
B = Bh*M;
B = B(:,1:3);
pcA = pointCloud(A);
pcB = pointCloud(B);
figure(1); clf;
pcshow(pcA,'MarkerSize',12);
hold on;
pcshow(pcB,'MarkerSize',12);

% print out in allensdk format
fprintf('T%s=[',F{l,1}); fprintf('%2.3f, ',M(:,1:3)'); fprintf(']\n');  %%% this one needs to be transposed

%T01747.swc=[0.343, -0.001, -0.023, 0.003, 0.343, 0.031, -0.063, 0.088, -0.957, 105.759, -28.403, 116.610]
%T01753.swc=[0.342, 0.021, 0.029, -0.032, 0.301, 0.163, 0.043, 0.461, -0.845, 125.749, -54.111, 13.573 ]
%T01755.swc=[0.344, 0.014, -0.007, -0.014, 0.343, 0.016, -0.022, 0.045, -0.962, 118.517, -49.650, 103.909, ]
%T01754.swc=[0.343, -0.017, -0.008, 0.019, 0.327, 0.104, -0.006, 0.293, -0.918, 103.590, -32.062, 65.042, ]
%T01748.swc=[0.344, 0.010, -0.002, -0.010, 0.343, 0.029, -0.007, 0.082, -0.960, 121.305, -56.248, 93.350, ]
%T01751.swc=[-0.343, 0.028, 0.016, 0.029, 0.341, 0.038, 0.036, -0.110, 0.956, 453.370, -34.837, -21.300, ]

%
% fix the transpose!!
%
swc{1}=[0.343,  -0.001, -0.023, 0.003, 0.343, 0.031, -0.063, 0.088, -0.957, 105.759, -28.403, 116.610]
swc{2}=[0.344,  0.010, -0.002, -0.010, 0.343, 0.029, -0.007, 0.082, -0.960, 121.305, -56.248, 93.350, ]
swc{3}=[-0.343, 0.028, 0.016, 0.029, 0.341, 0.038, 0.036, -0.110, 0.956, 453.370, -34.837, -21.300, ]
swc{4}=[0.342,  0.021, 0.029, -0.032, 0.301, 0.163, 0.043, 0.461, -0.845, 125.749, -54.111, 13.573 ]
swc{5}=[0.343,  -0.017, -0.008, 0.019, 0.327, 0.104, -0.006, 0.293, -0.918, 103.590, -32.062, 65.042, ]
swc{6}=[0.344,  0.014, -0.007, -0.014, 0.343, 0.016, -0.022, 0.045, -0.962, 118.517, -49.650, 103.909, ]

for i= 1:6
  A =reshape(swc{i},3,4);
  A(1:3,1:3) = A(1:3,1:3)';
  fprintf('T0.append([',F{l,1}); fprintf('%2.3f, ',A(:)); fprintf('])\n');
end 
  

