%T01747.swc=[0.343, -0.001, -0.023, 0.003, 0.343, 0.031, -0.063, 0.088, -0.957, 105.759, -28.403, 116.610]
%T01748.swc=[0.344, 0.010, -0.002, -0.010, 0.343, 0.029, -0.007, 0.082, -0.960, 121.305, -56.248, 93.350, ]
%T01751.swc=[-0.343, 0.028, 0.016, 0.029, 0.341, 0.038, 0.036, -0.110, 0.956, 453.370, -34.837, -21.300, ]
%T01753.swc=[0.342, 0.021, 0.029, -0.032, 0.301, 0.163, 0.043, 0.461, -0.845, 125.749, -54.111, 13.573 ]
%T01754.swc=[0.343, -0.017, -0.008, 0.019, 0.327, 0.104, -0.006, 0.293, -0.918, 103.590, -32.062, 65.042, ]
%T01755.swc=[0.344, 0.014, -0.007, -0.014, 0.343, 0.016, -0.022, 0.045, -0.962, 118.517, -49.650, 103.909, ]
F = {
'01747.swc','npf-M-200000.swc',  %y
'01748.swc','npf-M-300001.swc' , %y
'01751.swc','npf-M-200001.swc' , %y
'01753.swc','npf-M-300004.swc' , %
'01754.swc','npf-M-300005.swc' , %y
'01755.swc','npf-M-300006.swc',  %
};
swc{1}=[0.343,  -0.001, -0.023, 0.003, 0.343, 0.031, -0.063, 0.088, -0.957, 105.759, -28.403, 116.610];
swc{2}=[0.343,  -0.017, -0.008, 0.019, 0.327, 0.104, -0.006, 0.293, -0.918, 103.590, -32.062, 65.042, ];
swc{3}=[-0.343, 0.028, 0.016, 0.029, 0.341, 0.038, 0.036, -0.110, 0.956, 453.370, -34.837, -21.300, ];
swc{4}=[0.344,  0.010, -0.002, -0.010, 0.343, 0.029, -0.007, 0.082, -0.960, 121.305, -56.248, 93.350, ];
swc{5}=[0.342,  0.021, 0.029, -0.032, 0.301, 0.163, 0.043, 0.461, -0.845, 125.749, -54.111, 13.573 ];
swc{6}=[0.344,  0.014, -0.007, -0.014, 0.343, 0.016, -0.022, 0.045, -0.962, 118.517, -49.650, 103.909, ];

%
% verify last year's alignment
%
for l = 1:6
  A = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/%s',F{l,2}),' ',2,0);
  A = A(:,3:5);
  B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s',F{l,1}),' ',2,0);
  B = B(:,3:5);

  M =reshape(swc{l},3,4);
  Bh = [B ones(size(B,1),1)];
  Bt = M*Bh';
  Bt = Bt';
  Bt = Bt(:,[1 2 3]);
  pcA = pointCloud(A);
  pcB = pointCloud(Bt);
  trans{l} = pcregrigid(pcB,pcA);
  R = trans{l}.T(1:3, 1:3);
  t = trans{l}.T(4, 1:3)';
  M2 = [R' t];
  Btt = (M2*[Bt ones(size(Bt,1),1)]')';
  pcBtt = pointCloud(Btt);

  Mtot = [M2; 0 0 0 1]*[M; 0 0 0 1];
  swc2{l} = Mtot(1:3,:);

  figure(l); clf;
  subplot(1,2,1); pcshow(pcA,'MarkerSize',12); hold on; pcshow(pcB,'MarkerSize',12);
  subplot(1,2,2); pcshow(pcA,'MarkerSize',12); hold on; pcshow(pcBtt,'MarkerSize',12);
end

for l = 1:6
  A = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/%s',F{l,2}),' ',2,0);
  A = A(:,3:5);
  B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s',F{l,1}),' ',2,0);
  B = B(:,3:5);

  M =reshape(swc2{l},3,4);
  Bh = [B ones(size(B,1),1)];
  Bt = M*Bh';
  Bt = Bt';
  Bt = Bt(:,[1 2 3]);
  pcA = pointCloud(A);
  pcB = pointCloud(Bt);
  figure(l); clf;
  pcshow(pcA,'MarkerSize',12);  hold on;
  pcshow(pcB,'MarkerSize',12);
end


% print out in allensdk format which is row major
for i= 1:6
  A =reshape(swc2{i},3,4);
  A(1:3,1:3) = A(1:3,1:3)';
  fprintf('T[''%s''] = ([',F{i,1}(1:5)); fprintf('%2.3f, ',A(:)); fprintf('])\n');
end 
  

%
% run python
%

for l = 1:6
  A = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flyimages/swcs/%s.aligned.swc',F{l,1}(1:end-4)),' ',2,0);
  A = A(:,3:5);
  B = dlmread(sprintf('/home/fowlkes/bigdipa/neurontracing_tutorial/flycircuit/swc_reg/%s',F{l,2}),' ',2,0);
  B = B(:,3:5);

  pcA = pointCloud(A);
  pcB = pointCloud(B);
  figure(l); clf;
  pcshow(pcA,'MarkerSize',12); hold on; pcshow(pcB,'MarkerSize',12);
end

%
% updated transforms
%
%T['01747'] = ([0.343, 0.007, -0.062, 0.005, 0.298, 0.483, -0.023, 0.172, -0.831, 103.778, -44.393, 61.425, ])
%T['01748'] = ([0.343, -0.010, 0.018, 0.005, 0.284, 0.545, 0.011, 0.194, -0.795, 120.433, -45.392, 11.603, ])
%T['01751'] = ([-0.344, 0.010, 0.008, 0.008, 0.298, -0.484, 0.008, 0.173, 0.833, 461.976, -1.197, -65.350, ])
%T['01753'] = ([0.341, -0.042, 0.061, 0.008, 0.207, 0.771, 0.047, 0.272, -0.575, 125.887, -27.066, -49.235, ])
%T['01754'] = ([0.343, 0.018, -0.002, -0.012, 0.249, 0.662, -0.012, 0.236, -0.701, 100.747, -26.197, 8.680, ])
%T['01755'] = ([0.344, -0.015, -0.024, 0.017, 0.292, 0.507, 0.001, 0.181, -0.819, 118.409, -58.984, 29.117, ])

