% in R
%
%myneuron = dbl[fc_gene_name(id)]
%write.table(myneuron$"npfMARCM-M000049_seg001"$"points","points.csv")
%

A = dlmread('npfMARCM-M000049_seg001_flycircuit2.csv');
B = dlmread('npf-M-200000.csv');
Bh = [B ones(size(B,1),1)];

B = -B;
B = B - repmat(mean(B),287,1);
sB = diag(max(B)-min(B));
sA = diag(max(A)-min(A));
B = B*inv(sB)*sA;
B = B + repmat(mean(A),287,1);

BTh = [B ones(size(B,1),1)];

M = inv(BTh\Bh);

%
%
%


%
% estimated transform
%
A = dlmread('npfMARCM-M000049_seg001_flycircuit2.csv');
B = dlmread('npf-M-200000.csv');
B = B*diag([-0.626 -0.5403 -0.4187]) + repmat([284.36 97.06 42.23],size(B,1),1);

pcA = pointCloud(A);
pcB = pointCloud(B);
trans = pcregrigid(pcB,pcA);
figure(1); clf;
pcshow(pcA);
hold on;
pcshow(pctransform(pcB,trans))

M2 = M*trans.T;

%
% ok, all done
%

A = dlmread('npfMARCM-M000049_seg001_flycircuit2.csv');
B = dlmread('npf-M-200000.csv');
Bh = [B ones(size(B,1),1)];
Bs = Bh*M2;


figure(1); clf;
plot3(A(:,1),A(:,2),A(:,3),'b.'); hold on;
plot3(Bs(:,1),Bs(:,2),Bs(:,3),'r.');
grid on; axis image; axis vis3d;


sprintf('%2.3f, ',M2(:,1:3)')



%>> M2
%
%M2 =
%
%-0.6224    0.0077   -0.0112         0
%-0.0087   -0.5265    0.1214    0.0000
%0.0062   -0.0941   -0.4079    0.0000
%284.2747   95.6827   46.1675    1.0000
%

%>> M2(1:3,1:3)
%ans =
%
%-0.6224    0.0077   -0.0112
%-0.0087   -0.5265    0.1214
%0.0062   -0.0941   -0.4079

%>> M2(4,1:3)
%
%ans = 
%  284.2747   95.6827   46.1675





